package com.example.demo;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.sql.*;
import java.util.Arrays;
import java.util.Random;

public class Inventory {
    private JPanel panel1;
    private JButton form1Button;
    private JButton form2Button;
    private JButton form3Button;
    private JTabbedPane tabbedPane1;
    private JTextField txtItemName;
    private JComboBox comboBox1;
    private JButton saveButton;
    private JTable table1;
    private JTextField txtItemAmount;
    private JTextField txtUnitType;
    private JTextField txtItemDesc;
    private JTextField txtSKUCategory;
    private JList list1;
    private JButton refreshButton;
    private JButton importCSVButton;
    private JButton exportButton;
    private JComboBox comboBox2;
    private JCheckBox checkbox1;

    private boolean isProcessingEvent = false;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new JFrame("Inventory");
                Inventory inventory = new Inventory();
                frame.setContentPane(inventory.panel1);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.pack();
                frame.setVisible(true);
                frame.setMinimumSize(new Dimension(900, 500));
                frame.setResizable(false);

                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                int x = (screenSize.width - frame.getWidth()) / 2;
                int y = (screenSize.height - frame.getHeight()) / 2;
                frame.setLocation(x, y);

                inventory.panel1.setMinimumSize(new Dimension(200, 200));
            }
        });
    }

    //Database Connection Starts Here
    Connection conn;
    PreparedStatement pst;

    public void connect(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/kainan-ni-kulot", "root", "");//xampp
            System.out.println("Connection established!");
        } catch (ClassNotFoundException ignored) {

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    //Database Connection ends here

    public void table_load() {
        try {
            String sql = "SELECT * FROM tbl_inventory";

            pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel() {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };

            String[] columnNames = {"ID", "Item Name", "Item Code", "Quantity", "Unit Type", "Item Category", "Item Description"};

            for (String columnName : columnNames) {
                tableModel.addColumn(columnName);
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    rowData[i] = rs.getObject(i + 1);
                }
                tableModel.addRow(rowData);
            }


            table1.setModel(tableModel);

            table1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (!e.getValueIsAdjusting() && !isProcessingEvent) {
                        isProcessingEvent = true;

                        int selectedRow = table1.getSelectedRow();
                        if (selectedRow != -1) {
                            try {
                                int itemId = (int) tableModel.getValueAt(selectedRow, 0);
                                String description = fetchAdditionalInfo(itemId);

                                Object[] options = {"OK", "Update Stock", "Delete"};
                                int choice = JOptionPane.showOptionDialog(null, "Information:\n" + description, "Item Description",
                                        JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);

                                switch (choice) {
                                    case 1: // Update
                                        handleUpdate(itemId);
                                        break;
                                    case 2: // Delete
                                        handleDelete(itemId);
                                        break;
                                }
                            } catch (SQLException ex) {
                                throw new RuntimeException(ex);
                            } finally {
                                isProcessingEvent = false;  // Reset the flag here
                            }
                        }
                    }
                }
            });


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void handleUpdate(int itemId) {
        try {
            String sql = "UPDATE tbl_inventory SET item_number = ? WHERE id = ?";
            pst = conn.prepareStatement(sql);

            String newItemNumberStr = JOptionPane.showInputDialog(null, "Enter the new item stock:", "Update Item Number", JOptionPane.QUESTION_MESSAGE);

            if (newItemNumberStr != null && !newItemNumberStr.isEmpty()) {
                int newItemNumber = Integer.parseInt(newItemNumberStr);

                pst.setInt(1, newItemNumber);
                pst.setInt(2, itemId);

                int rowsAffected = pst.executeUpdate();

                if (rowsAffected > 0) {
                    int option = JOptionPane.showConfirmDialog(null, "Item number updated successfully. Do you want to see the new stock details?", "Update",
                            JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);

                    if (option == JOptionPane.YES_OPTION) {
                        String description = fetchAdditionalInfo(itemId);
                        JOptionPane.showMessageDialog(null, "New Stock Details:\n" + description, "Item Description", JOptionPane.INFORMATION_MESSAGE);
                    }

                    table_load();  // Refresh the JTable after updating
                } else {
                    JOptionPane.showMessageDialog(null, "Failed to update item number.", "Update",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (SQLException | NumberFormatException ex) {
            throw new RuntimeException(ex);
        }
    }


    private void handleDelete(int itemId) {
        try {
            int option = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this item?", "Confirm Deletion", JOptionPane.YES_NO_OPTION);

            if (option == JOptionPane.YES_OPTION) {
                String sql = "DELETE FROM tbl_inventory WHERE id = ?";
                pst = conn.prepareStatement(sql);

                pst.setInt(1, itemId);

                int rowsAffected = pst.executeUpdate();

                if (rowsAffected > 0) {
                    JOptionPane.showMessageDialog(null, "Item deleted successfully.", "Delete",
                            JOptionPane.INFORMATION_MESSAGE);
                    table_load();
                } else {
                    JOptionPane.showMessageDialog(null, "Failed to delete item.", "Delete",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void list_load() {
        try {
            String sql = "SELECT id, item_name, item_code, item_category FROM tbl_inventory";

            pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            DefaultListModel<String> listModel = new DefaultListModel<>();

            while (rs.next()) {
                int itemId = rs.getInt("id");
                String itemName = rs.getString("item_name");
                String itemCode = rs.getString("item_code");
                String itemCategory = rs.getString("item_category");

                String itemText = itemId + ">>> " + itemName + " ( " + itemCode + " ) " + " --> " + itemCategory;
                listModel.addElement(itemText);
            }

            list1.setModel(listModel);
            list1.setCellRenderer(new CustomListCellRenderer());


            list1.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (!e.getValueIsAdjusting()) {
                        SwingUtilities.invokeLater(() -> {
                            String selectedItem = (String) list1.getSelectedValue();

                            if (selectedItem != null) {
                                int itemId = extractItemId(selectedItem);

                                try {
                                    String additionalInfo = fetchAdditionalInfo(itemId);

                                    JTextField quantityField = new JTextField();
                                    Object[] message = {
                                            "Item: " + selectedItem.split(">>>")[1] + "\nDetails: " + additionalInfo,
                                            "\nEnter quantity to use:",
                                            quantityField
                                    };

                                    if (fetchCurrentItemNumber(itemId) == 0) {
                                        int option = JOptionPane.showOptionDialog(
                                                null,
                                                message,
                                                "Use Item",
                                                JOptionPane.OK_CANCEL_OPTION,
                                                JOptionPane.INFORMATION_MESSAGE,
                                                null,
                                                new Object[]{"Delete"},
                                                "Delete"
                                        );

                                        if (option == 0) {
                                            deleteItem(itemId);
                                            refreshData();
                                        }
                                    } else {
                                        int option = JOptionPane.showOptionDialog(
                                                null,
                                                message,
                                                "Use Item",
                                                JOptionPane.OK_CANCEL_OPTION,
                                                JOptionPane.INFORMATION_MESSAGE,
                                                null,
                                                null,
                                                null
                                        );

                                        if (option == JOptionPane.OK_OPTION) {
                                            try {
                                                int usedQuantity = Integer.parseInt(quantityField.getText());
                                                updateItemNumber(itemId, usedQuantity);
                                                refreshData(); // Refresh the data in the JList and JTable
                                            } catch (NumberFormatException ex) {
                                                JOptionPane.showMessageDialog(null, "Please enter a valid integer for Quantity.", "Error", JOptionPane.ERROR_MESSAGE);
                                            }
                                        }
                                    }
                                } catch (SQLException ex) {
                                    throw new RuntimeException(ex);
                                }
                            }
                        });
                    }
                }

            });


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    class CustomListCellRenderer extends DefaultListCellRenderer {
        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            label.setHorizontalAlignment(JLabel.CENTER);
            label.setVerticalAlignment(JLabel.CENTER);
            label.setOpaque(true);

            return label;
        }
    }

    private int extractItemId(String selectedItem) {
        String[] parts = selectedItem.split(">>>");
        return Integer.parseInt(parts[0]);
    }

    private String fetchAdditionalInfo(int id) throws SQLException {
        // Customize this SQL query based on your database structure
        String sql = "SELECT item_name, item_code, item_number, item_desc, unit_type, item_category FROM tbl_inventory WHERE id = ?";
        pst = conn.prepareStatement(sql);
        pst.setInt(1, id);
        ResultSet rs = pst.executeQuery();

        if (rs.next()) {
            int itemNumber = rs.getInt("item_number");
            String itemDesc = rs.getString("item_desc");
            String itemUnit = rs.getString("unit_type");
            String itemName = rs.getString("item_name");
            String itemCode = rs.getString("item_code");
            String itemCategory = rs.getString("item_category");

            return "Item Name: " + itemName + "\nSKU: " + itemCode + "\nCategory: " + itemCategory + "\nStock(s): " + itemNumber + itemUnit + "\nDescription: " + itemDesc;
        } else {
            return "No additional information available";
        }
    }

    private int fetchCurrentItemNumber(int itemId) throws SQLException {
        String sql = "SELECT item_number FROM tbl_inventory WHERE id = ?";
        pst = conn.prepareStatement(sql);
        pst.setInt(1, itemId);
        ResultSet rs = pst.executeQuery();

        if (rs.next()) {
            return rs.getInt("item_number");
        } else {
            throw new SQLException("Item not found");
        }
    }

    // Add this method to delete the item
    private void deleteItem(int itemId) {
        try {
            // Execute SQL query to delete the item
            String deleteSql = "DELETE FROM tbl_inventory WHERE id = ?";
            pst = conn.prepareStatement(deleteSql);
            pst.setInt(1, itemId);
            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Item deleted successfully!", "Success", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    private void updateItemNumber(int itemId, int usedQuantity) {
        try {
            int currentQuantity = fetchCurrentItemNumber(itemId);

            if (currentQuantity >= usedQuantity) {
                int newQuantity = currentQuantity - usedQuantity;

                String updateSql = "UPDATE tbl_inventory SET item_number = ? WHERE id = ?";
                pst = conn.prepareStatement(updateSql);
                pst.setInt(1, newQuantity);
                pst.setInt(2, itemId);
                pst.executeUpdate();

                String newStockMessage = "Item number updated successfully.\nStock Remaining: " + newQuantity;
                JOptionPane.showMessageDialog(null, newStockMessage, "Update Success", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Not enough quantity to use.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void refreshData() {
        table_load();
        list_load();
        JOptionPane.showMessageDialog(null, "Data updated successfully.", "Refresh", JOptionPane.INFORMATION_MESSAGE);
    }

    public void resetForm() {
        txtItemName.setText("");
        txtItemAmount.setText("");
        txtUnitType.setText("");
        txtItemDesc.setText("");
        txtSKUCategory.setText("");
        comboBox1.setSelectedIndex(0);
        comboBox2.setSelectedIndex(0);
    }

    public void comboBox(){
        for (String s : Arrays.asList("Select A Category", "Condiment", "Beverage", "Meat", "Food", "Merchandise", "Others"))
            comboBox1.addItem(s);
    }

    public void comboBox2() {
        try {
            // Clear existing items in comboBox2
            comboBox2.removeAllItems();

            // Add a default item to the combo box
            comboBox2.addItem("Select An Item Code");

            String sql = "SELECT item_code FROM tbl_inventory";
            pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                String itemCode = rs.getString("item_code");
                comboBox2.addItem(itemCode);
            }

            comboBox2.setEditable(false);

            updateComboBox2Visibility();

            // Add action listener to comboBox2
            comboBox2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        // Get the selected item code from comboBox2
                        String selectedItemCode = comboBox2.getSelectedItem().toString();

                        if (!selectedItemCode.equals("Select An Item Code")) {
                            // Fetch details from the database for the selected item code
                            String sql = "SELECT item_name, item_category, item_desc FROM tbl_inventory WHERE item_code = ?";
                            pst = conn.prepareStatement(sql);
                            pst.setString(1, selectedItemCode);
                            ResultSet rs = pst.executeQuery();

                            if (rs.next()) {
                                txtItemName.setText(rs.getString("item_name"));
                                txtItemDesc.setText(rs.getString("item_desc"));

                                if (comboBox1.getSelectedIndex() == 0) {
                                    comboBox1.setSelectedItem(rs.getString("item_category"));
                                    updateSKUCategory();  // Update SKU category when updating category from the database
                                } else {
                                    txtSKUCategory.setText("");  // Clear txtSKUCategory if comboBox1 is selected
                                }
                            }
                        } else {
                            // Handle the case when the default item is selected
                            txtItemName.setText("");
                            txtItemDesc.setText("");
                            txtSKUCategory.setText("");
                        }
                    } catch (SQLException ex) {
                        throw new RuntimeException(ex);
                    }
                }
            });


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void updateComboBox2Visibility() {
        comboBox2.setVisible(checkbox1.isSelected());
        txtSKUCategory.setEnabled(!checkbox1.isSelected());

        if (checkbox1.isSelected()) {
            txtSKUCategory.setText("");
        }
    }

    private String randomSKU;

    private String generateRandomSKU() {
        Random random = new Random();
        return String.format("%04d", random.nextInt(10000));
    }

    private String extractConsonants(String input, int count) {
        StringBuilder result = new StringBuilder();
        int consonantCount = 0;

        for (char c : input.toCharArray()) {
            if (consonantCount >= count) {
                break;
            }

            if (Character.isLetter(c) && !isVowel(c)) {
                result.append(c);
                consonantCount++;
            }
        }

        if (consonantCount == 3) {
            return result.toString();
        }

        result = new StringBuilder();
        for (char c : input.toCharArray()) {
            if (Character.isLetter(c)) {
                result.append(c);
            }

            if (result.length() == 3) {
                break;
            }
        }

        return result.toString();
    }

    private boolean isVowel(char c) {
        return "AEIOUaeiou".indexOf(c) != -1;
    }

    private String capitalizeFirstLetter(String input) {
        StringBuilder result = new StringBuilder();

        boolean capitalizeNext = true;
        for (char c : input.toCharArray()) {
            if (Character.isWhitespace(c)) {
                capitalizeNext = true;
                result.append(c);
            } else if (capitalizeNext) {
                result.append(Character.toTitleCase(c));
                capitalizeNext = false;
            } else {
                result.append(Character.toLowerCase(c));
            }
        }

        return result.toString();
    }

    private String getItemCodeIfExists(String itemName) {
        try {
            String sql = "SELECT item_code FROM tbl_inventory WHERE item_name = ?";
            pst = conn.prepareStatement(sql);
            pst.setString(1, itemName);
            ResultSet rs = pst.executeQuery();

            if (rs.next()) {
                return rs.getString("item_code");
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        return null;
    }

    private void updateSKUCategoryIfNotEmpty() {
        String itemName = txtItemName.getText();

        if (!itemName.trim().isEmpty()) {
            updateSKUCategory();
        } else if (comboBox1.getSelectedIndex() == 0) {
            txtSKUCategory.setText("");
        }
    }

    private void updateSKUCategory() {
        String itemName = txtItemName.getText().toUpperCase();
        String category = comboBox1.getSelectedItem().toString().toUpperCase();

        if (!itemName.trim().isEmpty() && !comboBox1.getSelectedItem().equals("Select A Category")) {
            if (randomSKU == null) {
                randomSKU = generateRandomSKU();
            }

            String skuCategory = extractConsonants(category, 3);

            skuCategory += extractConsonants(itemName, 3);

            skuCategory += randomSKU;

            txtSKUCategory.setText(skuCategory);

        } else if (comboBox1.getSelectedIndex() == 0) {
            txtSKUCategory.setText("");
        }
    }

    private void importCSV() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Choose CSV File");
        int userSelection = fileChooser.showOpenDialog(null);

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();

            try (BufferedReader reader = new BufferedReader(new FileReader(selectedFile))) {
                // Skip the header if your CSV file has one
                reader.readLine();

                String line;
                String sql = "INSERT INTO tbl_inventory (item_name, item_code, item_number, item_desc, unit_type, item_category) VALUES (?, ?, ?, ?, ?, ?)";
                pst = conn.prepareStatement(sql);

                while ((line = reader.readLine()) != null) {
                    String[] data = line.split(",");

                    pst.setString(1, data[0].trim());
                    pst.setString(2, data[1].trim());
                    pst.setInt(3, Integer.parseInt(data[2].trim()));
                    pst.setString(4, data[3].trim());
                    pst.setString(5, data[4].trim());
                    pst.setString(6, data[5].trim());

                    pst.executeUpdate();
                }

                JOptionPane.showMessageDialog(null, "CSV data imported successfully.", "Import CSV",
                        JOptionPane.INFORMATION_MESSAGE);
                refreshData();
            } catch (IOException | SQLException | NumberFormatException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    private void exportTableDataToCSV() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Save CSV File");
        fileChooser.setFileFilter(new FileNameExtensionFilter("CSV files (*.csv)", "csv"));

        int userSelection = fileChooser.showSaveDialog(null);

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            String filePath = selectedFile.getAbsolutePath();

            // Append ".csv" extension if not already present
            if (!filePath.toLowerCase().endsWith(".csv")) {
                filePath += ".csv";
            }

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
                // Write additional header
                writer.write("List of Inventory Items");
                writer.newLine();

                // Write custom header
                String[] headers = {"ID", "Item Name", "Item Code", "Quantity", "Unit Type", "Item Category", "Item Description" };
                for (int i = 0; i < headers.length; i++) {
                    writer.write(headers[i]);
                    if (i < headers.length - 1) {
                        writer.write(",");
                    }
                }
                writer.newLine();

                // Write data
                for (int row = 0; row < table1.getRowCount(); row++) {
                    for (int col = 0; col < table1.getColumnCount(); col++) {
                        writer.write(table1.getValueAt(row, col).toString());
                        if (col < table1.getColumnCount() - 1) {
                            writer.write(",");
                        }
                    }
                    writer.newLine();
                }

                JOptionPane.showMessageDialog(null, "Table data exported to CSV successfully.", "Export CSV",
                        JOptionPane.INFORMATION_MESSAGE);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Error exporting data to CSV.", "Export CSV",
                        JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace();
            }
        }
    }

    public Inventory() {
        connect();
        comboBox();
        comboBox2();

        table_load();
        list_load();

        form1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tabbedPane1.setSelectedIndex(0);
            }
        });

        form2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tabbedPane1.setSelectedIndex(1);
            }
        });

        form3Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tabbedPane1.setSelectedIndex(2);
            }
        });

        txtItemName.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSKUCategoryIfNotEmpty();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSKUCategoryIfNotEmpty();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateSKUCategoryIfNotEmpty();
            }
        });

        checkbox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (checkbox1.isSelected()) {
                    txtSKUCategory.setText("");
                }else{
                    updateSKUCategory();
                }
                updateComboBox2Visibility();

            }
        });


        comboBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (checkbox1.isSelected()) {
                    txtSKUCategory.setText("");
                } else if (comboBox1.getSelectedItem().equals("Others")) {
                    String customCategory = JOptionPane.showInputDialog(null, "Enter a category:");

                    if (customCategory != null && !customCategory.isEmpty()) {
                        comboBox1.addItem(customCategory);
                        comboBox1.setSelectedItem(customCategory);
                    } else {
                        comboBox1.setSelectedItem("Others");
                    }
                }

                updateSKUCategory();
            }
        });


        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String itemName, itemCategory, itemUnitType, itemDesc, itemNumber, itemCode;

                itemName = capitalizeFirstLetter(txtItemName.getText());
                itemUnitType = capitalizeFirstLetter(txtUnitType.getText());
                itemDesc = capitalizeFirstLetter(txtItemDesc.getText());

                itemCategory = comboBox1.getSelectedItem().toString();

                // Check if the entered itemCode already exists in the database
                itemCode = getItemCodeIfExists(itemName);

                if (itemCode == null) {
                    // Item does not exist, generate a new item_code
                    String skuCategory = extractConsonants(itemCategory, 3);
                    skuCategory += extractConsonants(itemName, 3);
                    skuCategory += randomSKU;

                    if (!skuCategory.contains("/")) {
                        skuCategory = skuCategory.substring(0, 3) + "/" + skuCategory.substring(3);
                    }

                    if (!skuCategory.contains("-")) {
                        skuCategory = skuCategory.substring(0, 7) + "-" + skuCategory.substring(7);
                    }

                    itemCode = skuCategory.toUpperCase();
                    txtSKUCategory.setText(itemCode);
                } else {
                    // Item exists, use the existing item_code
                    txtSKUCategory.setText(itemCode);
                }

                try {
                    itemNumber = txtItemAmount.getText();

                    pst = conn.prepareStatement("INSERT INTO tbl_inventory(item_name, item_category, item_number, unit_type, item_desc, item_code) VALUES (?,?,?,?,?,?)");
                    pst.setString(1, itemName);
                    pst.setString(2, itemCategory);
                    pst.setInt(3, Integer.parseInt(itemNumber));
                    pst.setString(4, itemUnitType);
                    pst.setString(5, itemDesc);
                    pst.setString(6, itemCode);
                    pst.executeUpdate();

                    JOptionPane.showMessageDialog(null, "Ingredients Added! SKU Code is " + itemCode, "Success", JOptionPane.INFORMATION_MESSAGE);
                    resetForm();

                    randomSKU = generateRandomSKU();

                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Please enter a valid integer for Item Number.");
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            }
        });


        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshData();
            }
        });

        importCSVButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                importCSV();
            }
        });

        exportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exportTableDataToCSV();
            }
        });


    }
}

module com.example.demo {
    requires javafx.controls;
    requires javafx.fxml;

    requires com.dlsc.formsfx;
    requires java.desktop;
    requires java.sql;
    requires mysql.connector.java;

    opens com.example.demo to javafx.fxml;
    exports com.example.demo;
}